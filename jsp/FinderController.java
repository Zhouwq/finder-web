@Controller
public class FinderController implements InitializingBean {
    @Resource
    ServletContext servletContext;

    private com.skin.finder.web.ActionDispatcher dispatcher;

    @Override
    public void afterPropertiesSet() throws Exception {
        this.dispatcher = new com.skin.finder.web.ActionDispatcher();
        this.dispatcher.setPackages(new String[]{"com.skin.finder.servlet", "com.skin.finder.admin.servlet"});
        this.dispatcher.init(this.servletContext);
    }

    @RequestMapping(value = "/finder")
    public void execute(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String loginURL = com.skin.finder.filter.SessionFilter.getLoginURL(request);

        if(com.skin.finder.filter.SessionFilter.check(request, response, loginURL)) {
            this.dispatcher.service(request, response);
        }
    }
}
