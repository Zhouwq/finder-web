<%@ page contentType="text/html; charset=utf-8"%>
<%@ page import="com.skin.finder.config.ConfigFactory"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<meta http-equiv="Pragma" content="no-cache"/>
<meta http-equiv="Cache-Control" content="no-cache"/>
<meta http-equiv="Expires" content="0"/>
<title>Finder - Powered by Finder</title>
<link rel="shortcut icon" href="?action=res&path=/finder/images/favicon.png"/>
<link rel="stylesheet" type="text/css" href="?action=res&path=/admin/css/form.css"/>
<script type="text/javascript" src="?action=res&path=/finder/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="?action=res&path=/admin/base.js"></script>
<script type="text/javascript" src="?action=res&path=/admin/security.js"></script>
</head>
<body contextPath="${contextPath}">
<div class="menu-bar">
    <a class="button" href="javascript:void(0)" onclick="window.history.back();"><span class="back"></span>返回</a>
    <a class="button" href="javascript:void(0)" onclick="window.location.reload();"><span class="refresh"></span>刷新</a>
    <span class="seperator"></span>
</div>
<div class="form">
    <div class="title"><h4>系统设置 - 安全设置</h4></div>
    <div class="form-row">
        <div class="form-label">超级管理员：</div>
        <div class="form-c400">
            <div class="form-field">
                <input name="adminName" type="text" class="text w300" placeholder="User Name" value="<%=ConfigFactory.getAdmin()%>"/>
            </div>
        </div>
        <div class="form-m300">
            <div class="form-comment">超级管理员账号。</div>
        </div>
    </div>
    <div class="form-row">
        <div class="form-label">安全Key：</div>
        <div class="form-c400">
            <div class="form-field">
                <input name="securityKey" type="text" class="text w300" placeholder="Security Key" value="<%=ConfigFactory.getSecurityKey()%>"/>
                <button id="url-test" class="button key-generate" for="securityKey">生成</button>
            </div>
        </div>
        <div class="form-m300">
            <div class="form-comment">Finder内部通讯使用的安全KEY。集群环境下，所有机器的安全KEY必须一致。强烈建议重新生成。</div>
        </div>
    </div>
    <div class="form-row">
        <div class="form-label">会话名：</div>
        <div class="form-c400">
            <div class="form-field">
                <input name="sessionName" type="text" class="text w300" placeholder="Session Name" value="<%=ConfigFactory.getSessionName()%>"/>
            </div>
        </div>
        <div class="form-m300">
            <div class="form-comment">会话存储的cookie名。</div>
        </div>
    </div>
    <div class="form-row">
        <div class="form-label">会话签名Key：</div>
        <div class="form-c400">
            <div class="form-field">
                <input name="sessionKey" type="text" class="text w300" placeholder="Session Key" value="<%=ConfigFactory.getSessionKey()%>"/>
                <button id="url-test" class="button key-generate" for="sessionKey">生成</button>
            </div>
        </div>
        <div class="form-m300">
            <div class="form-comment">会话签名使用的key。强烈建议重新生成。</div>
        </div>
    </div>

    <div class="form-row" style="display: none;">
        <div class="form-label">RSA PublicKey：</div>
        <div class="form-comment">用户登录使用的加密key。强烈建议重新生成。</div>
        <div class="form-field">
            <textarea name="publicKey" type="text" class="text" style="width: 800px; resize: none;"><%=ConfigFactory.getPublicKey()%></textarea>
        </div>
        <div class="form-field">
            <button id="url-test" class="button rsa-generate" for="sessionKey">生成</button>
        </div>
    </div>
    <div class="form-row" style="display: none;">
        <div class="form-label">RSA PrivateKey：</div>
        <div class="form-comment">用户登录使用的解密key。强烈建议重新生成。</div>
        <div class="form-field">
            <textarea name="privateKey" type="text" class="text" style="width: 800px; resize: none;"><%=ConfigFactory.getPrivateKey()%></textarea>
        </div>
    </div>
    <div class="button">
        <button id="submit" class="button ensure">保存并同步到集群</button>
    </div>
</div>
<div id="pageContext" style="display: none;"></div>
</body>
</html>
