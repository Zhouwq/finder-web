<%@ page contentType="text/html; charset=utf-8"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<title>Finder - Powered by Finder</title>
<meta http-equiv="Pragma" content="no-cache"/>
<meta http-equiv="Cache-Control" content="no-cache"/>
<meta http-equiv="Expires" content="0"/>
<link rel="shortcut icon" href="?action=res&path=/finder/images/favicon.png"/>
<link rel="stylesheet" type="text/css" href="?action=res&path=/finder/css/tab.css"/>
<script type="text/javascript" src="?action=res&path=/finder/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="?action=res&path=/finder/tab.js"></script>
</head>
<body>
<div id="welcome-panel" style="margin: 100px auto 0px auto; width: 400px; text-align: center; color: #c0c0c0;" href="javascript:void(0);">Welcome to Finder.</div>
<div id="tab-panel-container" class="tab-component" style="display: none;">
    <div class="tab-label-wrap"><ul></ul></div>
    <div class="tab-panel-wrap resize-d"></div>
</div>
<div id="pageContext" style="display: none;"></div>
</body>
</html>
