/*
 * $RCSfile: EditServlet.java,v $
 * $Revision: 1.1 $
 *
 * Copyright (C) 2008 Skin, Inc. All rights reserved.
 *
 * This software is the proprietary information of Skin, Inc.
 * Use is subject to license terms.
 */
package com.skin.finder.servlet;

import java.io.File;
import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.skin.finder.Finder;
import com.skin.finder.acl.AccessController;
import com.skin.finder.cluster.Agent;
import com.skin.finder.cluster.WorkspaceManager;
import com.skin.finder.util.Ajax;
import com.skin.finder.util.IO;
import com.skin.finder.util.Path;
import com.skin.finder.web.UrlPattern;
import com.skin.finder.web.servlet.BaseServlet;
import com.skin.finder.web.util.CurrentUser;

/**
 * <p>Title: EditServlet</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2006</p>
 * @author xuesong.net
 * @version 1.0
 */
public class EditServlet extends BaseServlet {
    private static final long serialVersionUID = 1L;
    private static final Logger logger = LoggerFactory.getLogger(EditServlet.class);

    /**
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    @UrlPattern("finder.edit")
    public void edit(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        if(Agent.dispatch(request, response)) {
            return;
        }

        String userName = CurrentUser.getUserName(request);
        String workspace = request.getParameter("workspace");
        String path = request.getParameter("path");
        String home = Finder.getWork(request, workspace);

        if(home == null) {
            Ajax.error(request, response, 403, "Workspace not exists.");
            return;
        }

        if(WorkspaceManager.getReadonly(workspace)) {
            Ajax.error(request, response, 403, "Workspace is readonly.");
            return;
        }

        String realPath = Finder.getRealPath(home, path);

        if(realPath == null) {
            Ajax.error(request, response, 404, path + " not exists !");
            return;
        }

        if(!AccessController.getWrite(userName, workspace, path)) {
            Ajax.denied(request, response);
            return;
        }

        String parent = Path.getParent(path);

        if(!AccessController.getWrite(userName, workspace, parent)) {
            Ajax.denied(request, response);
            return;
        }

        File file = new File(realPath);
        logger.info("edit: {}", file.getAbsolutePath());
    }

    /**
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    @UrlPattern("finder.save")
    public void save(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        if(Agent.dispatch(request, response)) {
            return;
        }

        String userName = CurrentUser.getUserName(request);
        String workspace = request.getParameter("workspace");
        String path = request.getParameter("path");
        String content = request.getParameter("content");
        String home = Finder.getWork(request, workspace);

        if(home == null) {
            Ajax.error(request, response, 403, "Workspace not exists.");
            return;
        }

        if(WorkspaceManager.getReadonly(workspace)) {
            Ajax.error(request, response, 403, "Workspace is readonly.");
            return;
        }

        String realPath = Finder.getRealPath(home, path);

        if(realPath == null) {
            Ajax.error(request, response, 404, path + " not exists !");
            return;
        }

        if(!AccessController.getWrite(userName, workspace, path)) {
            Ajax.denied(request, response);
            return;
        }

        String parent = Path.getParent(path);

        if(!AccessController.getWrite(userName, workspace, parent)) {
            Ajax.denied(request, response);
            return;
        }

        File file = new File(realPath);

        if(content != null) {
            IO.write(file, content.getBytes("utf-8"));
        }
        else {
            IO.touch(file);
        }
        Ajax.success(request, response, "true");
        return;
    }
}
